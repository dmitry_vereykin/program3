package com.dvereykin.program3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Dmitry Vereykin aka eXrump on 9/29/2016.
 */

class PersonalDataIntent {
    String name;
    String address;
    String city;
    String state;
    String zipCode;

    enum ActionType {
        ADD,
        EDIT,
        DELETE,
        VIEW
    }

    ActionType action;
    int addressIndex = 0;
    private Intent intent;

    PersonalDataIntent(Intent intent) {
        Bundle bundle = intent.getExtras();

        try {
            name = bundle.getString("name");
            address = bundle.getString("address");
            city = bundle.getString("city");
            state = bundle.getString("state");
            zipCode = bundle.getString("zipCode");
            action = ActionType.values()[bundle.getInt("action",0)];
            addressIndex = bundle.getInt("addressIndex");
        }
        catch (Exception ex) {
            ex.getMessage();
        }
    }

    PersonalDataIntent() {
        name = "";
        address = "";
        city = "";
        state = "";
        zipCode = "";
    }

    PersonalDataIntent(AddressAttributeGroup addressAttributes, ActionType action, int addressIndex) {
        name = addressAttributes.name;
        address = addressAttributes.address;
        city = addressAttributes.city;
        state = addressAttributes.state;
        zipCode = addressAttributes.zipCode;
        this.action = action;
        this.addressIndex = addressIndex;
    }

    void clearIntent() {
        intent = null;
    }

    private void putExtras() {
        intent.putExtra("name", name);
        intent.putExtra("address", address);
        intent.putExtra("city", city);
        intent.putExtra("state", state);
        intent.putExtra("zipCode", zipCode);
        intent.putExtra("action", action.ordinal());
        intent.putExtra("addressIndex",addressIndex);
    }

    Intent getIntent() {
        if (intent == null) {
            intent = new Intent();
            putExtras();
        }
        return intent;
    }

    Intent getIntent(Activity EditValuesActivity, Class<EditValuesActivity> activity) {
        if (intent == null) {
            intent = new Intent(EditValuesActivity, activity);
            putExtras();
        }
        return intent;
    }

}
